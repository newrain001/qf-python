# 1、注释说明
"""
'''
多行
注释
'''

# 单行注释
print('hello world')
print(__doc__,"写在文档最开头的多行注释，一般是一个模块帮助文档")
import random
print(random.__doc__)
"""


# 2、位置参数
"""
import sys
print(sys.argv)
print(sys.argv[1])
"""

# 3、获取用户标准输入
"""
r = input("请输入你的账号：")
print(r)
"""

# 4、变量的定义
a = 0
b = 1
"""
c = a
a = b 
b = c
print(a,b)
"""
"""
多源赋值1
a,b = b,a
print(a,b)
"""
"""
a, *_, b = "hello"
print(a, b)
"""

# a = 0
# print(a)
# print(type(a))
# print(id(a))

# 算数运算
'''
+ - * / // ** %
'''

# 比较运算
'''
为 False 的值有: 0 "" [] {} set() None False
== != > < >= <=

a = 1000
if a:
    print("小于")
else:
    print("大于")
'''

# 赋值运算
'''
= += -= *=
'''

# 逻辑运算
'''
a = 5
b = 3
if not ( a < 10 or b > 5):
    print(True)
else:
    print(False)
'''

# 成员运算
# print("ho" not in "hello")

# 判断内存地址是否相同
'''
a = 1
b = 1
print(a is not b)
print(id(a),id(b))
'''