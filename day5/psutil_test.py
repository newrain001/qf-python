# 登录状态 0 未登录 1 登录
login_status = 0

# 装饰器，检查登录状态
def outter(f):
    def inner():
        global login_status
        if login_status == 0:
            print("检测到你还没有登录，请先登录")
            input("请输入你的姓名：")
            login_status = 1
            f()
        else:  
            f()
    return inner

@outter  
def func1():
    print("=================")
    print("查看我的购物车")
    print("=================")
# func1 = outter(func1)

@outter
def func2():
    print("=================")
    print("查看我的个人信息")
    print("=================")
# func2 = outter(func2)

while True:
    print("1、查看购物车\n2、查看个人信息\n3、退出登录")
    r = input()
    if r == "1":
        func1()
    elif r == "2":
        func2()
    elif r == "3":
        print("退出登录成功")
        login_status = 0