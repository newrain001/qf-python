# find 指令 find 目录 -name "*关键字*"
import os

keyword = input("请输入你要查找的关键字：")
dir = input("请输入你要查找的目录：")
sep = "/" if os.name == "posix" else "C:\\"
dirpath = dir if dir else sep  # 如果不输入input，默认查找的目录

data_keyword = []
data_all = []
a = 0
# keyword = "pdf"
# dirpath = r"C:\Users\mingyu\Desktop\2401"


def find_file(dirpath):
    try:
        global a
        os.chdir(dirpath)
        dirlist = os.listdir()
        for i in dirlist:
            a += 1
            print(f"以查找到{a}个文件")
            if "proc" in i:
                continue
            dirname = os.path.join(dirpath, i)
            data_all.append(dirname)
            if keyword in i:
                print(f"以查找到关键字文件{dirname}")
                data_keyword.append(dirname)
            if os.path.isdir(dirname):
                find_file(dirpath=dirname)
    except PermissionError as e:
        print("权限不足，拒绝访问", e)


find_file(dirpath=dirpath)
for i in data_keyword:
    print(i)
print(f"查找到{len(data_keyword)}个关键字文件")
print(f"共查找{len(data_all)}个文件")
